#python 3.5.2

print ("Hello, world!")

# esto debería ser int
print(type(2))

# esto deberia float
print(type(2.0))

#esto debería string
print(type("ola k tal"))

# test 1 int to float
var_1  = 2 + 2.55
print(type(var_1))

# test 1 
print(int(var_1))

#test3 str a float
var_2 = "2.4"
print(float(var_2))

#test4 str a int
var_3 = "566"
print(str(var_3))

#test4 str a float   ---> error
#var_4 = "asdf"
#print(float(var_4))

#test5 str + int   ---> error
#var_5 = 2 + var_3




##################  OPERADORES ####################

var_6 = 1 +3
var_7 = 2.0*3
var_8 = 2-3
var_9 = 7/3
var_10 = 10/5
# La división de enteros es float
print(type(var_9), var_9, "otra cosa mas para imprimir")
print(type(var_10))

# x ** y es x^y
var_11 = var_7 ** 4
print("el valor de var_11 es ", var_11)

# x % y  da el resto de dividir x entre y
var_12 = 35%3
print(var_12)

# x // y es el cociente de dividir x entre y (número entero)
var_13 = 35//3
print(var_13)

# Los parentesis dan prioridad a las operaciones y el orden de la prioridad 
# Sigue el orden normal de la matemáticas
var_14 = (3*15) ** 4 + 300//7
print(var_14)

# Tres operaciones sobre strings
#Suma -> concatena
var_15 = "ola k " + "ase"
print(var_15)

#Multiplicacion por un entero es repeticion
var_16 = " ola k ase "
var_16 = var_16 * 16
var_16 *= 16
print(var_16)

# con string[x] se accede al elemento x de string
print("el tercer elemento de noseké es", var_16[2])

####################### CONTROL DE FLUJO ####################

# bucles for.
# [0, 50)
for i in range(50):
    print(i)
print(" -------------------------- ")
# [34, 49)
for j in range(34, 49):
    print(j)
print(" -------------------------- ")
for k in range(10, 101, 7):
    print(k)

# Condicional IF

if True:
    print("esto pasará")
if 5/7 > 1:
    print("esto  no pasará" )
else:
    print("pero esto si, como consecuencia")

################## EJERCICIO ###########################

for h in range(101):
    if h%7==0:
        print(h, "es múltiplo de siete")
    else:
        print(h)
