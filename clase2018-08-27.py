import math
s = "* * * \n* * *\n* * *\n\n"
print(s)

def print_square(n):
    cadena = ""
    for j in range(n+1):
        for i in range(n+1):
            cadena += "* "
        cadena += "\n"
    print(cadena)
print_square(3)


"""
i -> 2*i-1

1:*
2:***
3:*****
4:*******   
5:********   

"""
def print_piramide(n):
    for i in range(n+1):
        print((n-i)*" " + (2*i-1)*"*")

print_piramide(5)
"""
n es el numero de filas totales
i es la fila donde estoy

n-i
1:    *
2:   ***
3:  *****
4: *******
5:*********   

"""
def print_piramide_c(n):
    for i in range(n+1):
        print((2*i-1)*"*")
"""
1:    *
2:   ***
3:  *****
4: *******
5:*********  j
6: *******  :0      (n-2)-j*2
7:  *****   :1
8:   ***    :2      n = 9
9:    *     :3      n//2+1 =5
"""

def rombo(n): # n es SIEMPRE impar
    semi_alto = n//2+2
    for i in range(1, semi_alto):
        print((semi_alto-i)*" " + (2*i-1)*"*")
    for j in range(1, n-n//2+1):
        print((j+2)*" " + + ((n-2)-j*2)*"*")

print("rombo")
rombo(9)
print("rombo")

"""
Deberes: 

Ejercicio 1:
 Repetir el ejercicio doble pirámide pero con criterio consistente,
 dibujando un esquema y toda la pesca. Cuidado con la funcion range pq empieza
 en el 0(los asteriscos empiezan en el 1). Dibujame un esquema. 
 """
def rombox(altura):
    semialto = altura//2+2
    for fila in range(1, semialto):
        print((semialto-fila)*" " + (2*fila-1)*"*")
    for fila in range(1, altura-semialto-1):
        print(fila*2*" " + (altura-2-fila*2)*"*")
    
rombox(11)
        

"""
 
 
Ejecicio 2: 
def doble_piramide_lateral(n):

 
fila1   *    *
fila2  ***  ***
fila3 ********** -> 
"""
def doblepiramide(n):
    for f in range(1, n+1):
        print((n-f)*" " + (2*f-1)*"*" + 2*(n-f)*" " + (2*f-1)*"*")
        
doblepiramide(10)

#https://gitlab.com/lasdasdas/pybro

class Rectangulo:
    def __init__(self, b, a):
        self.base = b
        self.altura = a
        
    def area(self):
        self.area = self.base*self.altura
        return self.area



class Point:
    def __init__(self, x_arg, y_arg):
        self.x = x_arg
        self.y = y_arg
        
    def print_pretty(self):
        print("x , y : ",self.x,",", self.y)

        
    def distansia(self, pt2):
        dist = math.sqrt((self.x-pt2.x)**2 + (self.y-pt2.y)**2)
        return dist
    
    def mult(self, m):
        self.x *= m
        self.y *= m
    
    def dot(self, pt2):
        esc = self.x * pt2.x + self.y * pt2.y
        return esc
    
    def angulo(self):
        sen = self.y/(self.y**2 + self.x**2)
        angulo = math.asin(sen)*360/(2*math.pi)
        return angulo
     
    def mod(self):
        return math.sqrt(self.x**2 + self.y**2)
        
class Rectanguloxy:

        def __init__(self, plb, prt):
            self.plb = plb
            self.prt = prt
            
        def __str__(self):
            str_ret = "Obj rectángulo-> lb: (" + str(self.plb.x) + ", " + str(self.plb.y) +") rt: (" + str(self.prt.x) +", " + str(self.prt.y)+")"
            return str_ret
            
        def perimetro(self):
            return ((self.prt.x-self.plb.x)+(self.prt.y-self.plb.y))*2
        #ejercicio 0 
        def area(self):
            return (self.prt.x-self.plb.x)*(self.prt.y-self.plb.y)
        
        """
        def ejemplo_return_doble(self):
            return 12, 8
        
v_doce , v_ocho = rxy1.ejemplo_return_doble()
print(v_doce)
print(v_ocho)
       """
        
        #Ejercicio 1
        def intersection(self, rect):
        #Return True si se solapan self y rect
            return (rect.plb.x < self.prt.x) and (rect.plb.y < self.prt.y)
         
        #Ejercicio 2
        def intersection_rect(self, rect):
        # Return True si se solapan y ademas el rectangulo que produce el solapamiento
        # Lo que quiero es un rectángulo!!!!
            if (rect.plb.x < self.prt.x) and (rect.plb.y < self.prt.y):
                rectintersection = Rectanguloxy(Point(rect.plb.x, rect.plb.y), Point(self.prt.x, self.prt.y))
                return rectintersection
            else:
                print("los rectangulos no se solapan")
                return None
        
        # Ejercicio 3
        def union(self, rect):
        # Solamente el area de dos rectantulos que intersecan:
            intersc = self.intersection_rect(rect)
            return self.area() + rect.area() + intersc.area()
"""
  _________________ (4, 3)(prt)
 |                 |
 |                 |
 |                 | 
 |                 | 
 __________________
(2,2) 

"""        
            
rxy1 =Rectanguloxy(Point(0,0), Point(3,4))
rxy2 =Rectanguloxy(Point(2,2), Point(8,8))


print(rxy1.intersection(rxy2)) #tendría que ser True
print(rxy1.perimetro())
print(rxy2.area())
print(rxy1.intersection_rect(rxy2))
print(rxy1.union(rxy2))
print(rxy1)
