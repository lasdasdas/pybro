def max_3(n1, n2, n3):
    print("el input es ", n1, n2, n3)
    t = max(n1, n2)
    t2 = max(t, n3)
    return t2

a = max_3(5, 8, 7)
#  a vale 8

def arect(b, a):
    area = b*a
    return area

def fizzbuzz(n):
    for i in range(1, n+1):
        if (i%3 == 0) and (i%5 == 0):
            print("FizzBuzz")
        elif i%3==0: # elif means else if
            print("Fizz")
        elif i%5==0:
            print("Buzz")
        else:
            print(i)
            
def checktonto(a1, a2):
    if a1>0:
        print(a1, "es mayor que cero")
    elif a2>0:
        print(a2, "es mayor que cero pero", a1, "no")
    else:
        print("WARNING", a1, "y", a2, "son menores o igual que cero")


def factorial(n):
    if n>0 and n<25:
        res = 1
        for i in range(1, n+1):
            res *= i
        return res

def isprime(a):
    for i in range(2, a):
        if a%i==0:
            return False
    return True


#una funcion arearectángulo que tiene por argumentos base y altura

ej = arect(854, 4)
print(ej)

# Fizzbuzz 
# Es una funcion que cuando la llamo de doy el argumento n
# Imprime los numeros de 1 a n ambos inclusive. 
# Los numeros que son multiplos de 3 en vez del numero imprime Fizz
# Los numeros que son multiplos de 5 en vez del numero imprime Buzz
# Los numeros que son multiplos de 5 y de 3 en vez del numero imprime FizzBuzz

#fizzbuzz(40)

#mirar si a1 es mayor que 0 y solo si no lo es mirar si a2 lo es
a1 = 0
a2 = 0
checktonto(a1,a2)


#una función que devuelve el factorial de un número
#  m > 0 y m <25 pq max size of int es 9223372036854775807
q = factorial(31)
print(q)


# Is prime, return True if the number is prime. Return false otherwise.

a = 24
a_bool_prime = isprime(a)
if a_bool_prime:
    print("era prime")
else:
    print("no prime")
