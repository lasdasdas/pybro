#include <iostream>
#include <vector>
#include <array>


// Vector
// elementos continuos en la memoria
// Big O -> complejidad
// Acceso -> vec[n]   -> O(1) -> tarda siempre lo mismo
// Meter valores, por detrás O(K)  push_back -> tarda más cuando hay que realocar el vector o pedir memoria
// erase() borrar alguno de los elementos. O(n) -<


//otras estructuras

//deque  -> para insertar elementos por delante y por detras de forma eficiente (es en general más lento que vector)
//queue  -> igual que deque
//list   -> esta estructura está dise>ñada para poder insertar cosas en medio de forma eficiente O(1) pero el resto de operaciones son muy malas
//       ->  4-------------> 5------------------>  el acceso es hiper lento O[q](siendo q la posición a acceder

//array  -> vector DE TAMAÑO CONSTANTE


//  4 5 68 4 1 57  5 68 | | 4 1 3 7 8 7 9 7 97 7 9  ->O(n²)






bool ladrillos(int b, int s, int p);
void print_vector(std::vector<int> a);
long unsigned int fibonacci_vec(int n);
bool is_sorted(std::vector<float> vec);
template<typename T>
void print_vec_all(std::vector<T> i){
    for (auto a : i){
        std::cout << a << std::endl;
    }
}


int main(){
    
    std::vector<float> vec;
    //append
    vec.push_back(4.1);
    vec.push_back(5.0);
    vec.push_back(6.6);
    vec.push_back(7.0);
    //pop
    vec.pop_back();
    //borrar una posición concreta
    vec.erase (vec.begin()+2);
    
    std::array<int, 5> arr;
    
    std::vector<int> v({4,2,4,5,2,1,43,3,3});
    print_vec_all(v);
    print_vector(v);
    
    for(int i = 0; i<=10; i++){
        std::cout << fibonacci_vec(i) << ",  ";
    }
    
    int qtt = 50;
    {
         std::cout << qtt << std::endl;

    }

    std::cout << qtt << std::endl;
    std::vector<float> v1({1.0, 2.0, 3.0, 3.5, 4.7, 5.1, 6.0});
    std::vector<float> v2({1.0, 2.0, 23.0, 35.5, 14.7, 5.1, 6.0});
    std::cout << is_sorted(v1) <<std::endl;
    std::cout << is_sorted(v2) <<std::endl;
    
    if(ladrillos(1,5,6)){
        std::cout << "se puede construir la pared" << std::endl;
    }
    else{
        std::cout << "no se puede construir la pared" << std::endl;
    }
}


bool is_sorted(std::vector<float> vec){
    for(int i = 0; i < (vec.size()-1); i++){
            if(vec[i]  > vec[i+1]){
                return false;
            }
    }
    return true;
}

/*
std::vector<float> sort(std::vector<float> vec){
    for(int i = 0; i <= vec.size(); i++){
        if 
    }
}
*/


bool ladrillos(int b, int s, int p){
    float bigmax = p/b;
    for(int i=0; i<bigmax; i++){
        int sobrante = p - b*i;
        if(sobrante%s==0){
            return true;
        }
    }
    return false;
}

void print_vector(std::vector<int> a){
    for(int i = 0; i <= a.size(); i++){
        std::cout << a[i] << ",  ";
    }
    std::cout << std::endl;
}

long unsigned int fibonacci_vec(int n){ //n > 2
    std::vector<long unsigned int> fib({1, 1});
    for(int i = 0; i<=n; i++){
        long unsigned int next_val = fib[fib.size()-1] + fib[fib.size()-2];
        fib.push_back(next_val);
    }
    return fib[n]; 
}

/*
#include <iostream>
#include <vector>

int popmin(std::vector<int> v){
    int minvalue = v[0];
    for(int i=0; i<v.size(); i++){
        if(v[i]<minvalue){
            int minvalue = v[i];
            int minindex = i;
        }
    }
    v.pop_back(minvalue);
    return minvalue;
}

std::vector<int> vec({1,2,5,0,4,7});

int main()
{

popmin(vec);
    
}

