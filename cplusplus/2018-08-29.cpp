//g++  5.4.0
// c++ se compila
// tiene un tipado mucho fuerte. Es mucho más estricto con los tipos. Y con sus conversiones. 
//C++ es lenguaje mucho más potente  <----------------------------------------> c++ es menos dinámico

//               Gestionas tu memoria  | Colector de basura       |    Interpretados (No se compilan)         
// es más potente < -------------------------------------------------------------------------> es más dinámico
//                 ^       |   |         |                              |             |
//           Ensamblador   |   |        Java                         Matlab/Octave    |
//                         C  C++                                                   Python                                               

// c++ es más modular (Está separado en más paquete)



// c++ está pensado para dispositivos de muy poca potencia. 
// c++ tiene un altísimo rendiminto y versatilidad. Se usa en videojuegos, mining, en bolsa en aviónica, en machine learning, procesamiento de sonido, robótica.
// c++ es muy complicado. Tiene muchisimas herramientas que hay que saber usar bien. El rendimiento importa(pero a tí de momento no)
// ha perdido un poco de importancia frente a leguajes más dinámicos. 


//los comentarios van así

/* o asi
   se pueden 
   poner multilinea */

// Java está hiper orientado al paradigma OOP -> Object Oriented Programming  LAS CLASES
//C++ también pero también admite MUY BIEN el resto de paradigmas, (funcional, ala C)
//C++ era (y más o menos es) un superconjunto de C. Es C + muchas más cosas. C++ nació como "C con clases


#include <iostream>  // <- Importar el paquete de imprimir
#include <string>

int main()
{
    std::cout << "Hello, world!\n";
    std::cout << "asfasd! " << 5 << "asfdas\n";
    std::cout << "asfasd! " << 5 << "asfdas" << std::endl;
    
    int a; //(Por lo menos 4 bytes, pero puede tener más, es un poco confuso ) 
    long int b;
    float c;
    double d;
    char e;
    std::cout << sizeof(a) << "\n";
    std::cout << sizeof(b) << "\n";
    std::cout << sizeof(c) << "\n";
    std::cout << sizeof(d) << "\n";
    std::cout << sizeof(e) << "\n";
    
    
    //las variables se declaran Y se inicializan ---> Si no lo haces casi seguro que vas a tener muchos problemas
    
    int var1;
    std::cout <<"Variable sin inicializar   "<< var1<< "\n";
    var1 = 123;
    std::cout <<"Variable inicializar   "<< var1<< "\n";
    
    //Se puede hacer en la misma linea.
    int  var2 = 0;
    
    // ONE DEFINITION RULE. Solamente se definen una vez las variables
    int var3 = 0;
    //int var3; // Esto no
    
    
    //Para pasar de un tipo a otro se usa la opreción de cast
    
    //Cast de tipo C
    
    int cast1 = 4;
    double cast2 = 200.5;
    std::cout <<"cast2 es  "<< cast2<< "\n";
    cast2 = (double) cast1;
    std::cout <<"cast2 es  "<< cast2<< "\n";
    
    //Cast de tipo C++
    cast1 = 40;
    cast2 = static_cast<double>(cast1);
    std::cout <<"cast2 es  "<< cast2<< "\n";


    
    
    //El float puede albergar  +-3.4E+38 
    //El double  +1.7E+308
    //Cuando haces casting, puedes perder precision, mucho cuidado. UB  Undefined Behaviour
    
    /*
    for(int i =0; i<1000; i++){
        int8_t chch = i;
        //std::cout << (int)chch << std::endl;
    }
    */
    //Hacer casting puede significar perder resolución.
    int var11 =23423423;
    int8_t var12;
    
    var12 = static_cast<int8_t>(var11);
    std::cout << (int)var12 << std::endl;
    var12 = 45;
    
    //El caso inverso, es seguro
    var11 = static_cast<int8_t>(var12);
    std::cout << var11 << std::endl;
    
    //El tipo bool -> en ppo soolo un bit, en la realidad son 4;
    
    bool ola;
    std::cout <<"Size of bool"<< sizeof(bool) << std::endl;
    ola = 1; // Los cast no tinenen que ser explicitos, pero también son peligrosos en algunos casos.
             //Cast de int a bool int !=0 si int no es 0-> true el resto incluso negativos son true
    
    float test1 = 4;
    int test2 = 8;
    test1 = test2;
    std::cout <<"test1 es   "  <<test1 << std::endl; //Esto también funciona. 


    
    //Las cadenas son un rollo;
    char achar = 47;
    achar = 'q';
    std::cout << achar << std::endl;

    
    
    //Control de flujo
    
    //for
    for(int i=0; i < 50; i++){
       std::cout << i << std::endl;
    }
    
    //while 
    int i = 0;
    
    
    // 1(Division entera)5   -> Cociente: 0 Resto 1
    
    while (i < 50){
        i++;
        //if 
        if( i % 3 == 0){
            continue;
            std::cout << " treees "<< i << std::endl;
        }else if(i % 5 == 0){
            std::cout << "cincooo "<< i << std::endl;
            break;
        }else{
            std::cout << "otro "<< i << std::endl;
        }
    }
 }
