#include <iostream>

void Highestof3(int a, int b, int c){
    
    if(b<a && c<a){
        std::cout << "el número más alto es " << a;
    }
    if(a<b && c<b){
        std::cout << "el número más alto es " << b;
    }
    if(a<c && b<c){
        std::cout << "el número más alto es " << c;
    }
    if(a == b && b == c){
        std::cout << "los tres números son iguales";
    }
}


void Fibonacci(int n){
   
    //imprime números de fibonatxi hasta la posición n en la serie
    int e1 = 1, e2 =1, siguiente = 0;
    for(int i=0; i<=n; i++){
        if(i == 0){
        std::cout << e1 << "\n" << std::endl;
        continue;
        }
        if(i == 1){
        std::cout << e2 << "\n" << std::endl;
        continue;
        }
        //el siguiente elemento de la serie es la suma de los dos anteriores
        //calculo el valor de "siguiente" sumando los dos primeros elementos de la serie
        //le asigno al elemento 1 el valor del elemento 2, y al elemento 2 le asigno el valor de la variable siguiente
        
        siguiente = e1 + e2;
        e1 = e2;
        e2 = siguiente;
        std::cout << siguiente << "\n" << std::endl;
    }
     
}

int main(){
    
    Highestof3(7, 9, 4);
    Fibonacci(10);
}
