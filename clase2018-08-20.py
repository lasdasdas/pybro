
#3 + 8 = 24   ->> 8//3 = cociente * divisor + resto = dividendo
def make_bricks(small, big, goal):
    maxbig = goal // big
    for i in range(maxbig+1):
        dispon = goal - i*big
        if dispon%small == 0:
            numbig = i
            numsmall = dispon/small
            print("el número de ladrillos grandes utilizados es", numbig)
            print("el número de ladrillos pequeños utilizados es", numsmall)
            return True
    return False

print("caso 1 -> ", make_bricks(7,8,343243))


def any_equal_3(a,b,c):
    """
    True is dos de ellos son iguales
    False is son todos distintos
    """ 
#    if a == b or b == c or c == a:
#        return True
#    else:
#        return False   True or True or False
    #Así es mucho más elegante
    return a == b or b == c or c == a

# [4, 5 , 2 ,1 ,2 ,123, 112, 11]
def any_equal_n(v):
    """
    True is hay dos elementos iguales
    False is son todos distintos
    """
    for i in range(len(v)):
        for j in range(i+1, len(v)):
            if v[i] == v[j]:
                return True
    return False

print(any_equal_3(1,2,1))
print(any_equal_3(1,2,3))

print(any_equal_n([4, 5, 7, 4, 8]))
print(any_equal_n([4, 5, 7, 3, 8]))

"""
Given 3 int values, a b c, return their sum. However, if one of the values is the same as another of the values, it does not count towards the sum.
"""

def lone_sum(a,b,c):
    summ = 0
    if a == b and b == c:
        summ = a
    elif a == b:
        summ = a + c
    elif b == c:
        summ = a + b
    elif c == a:
        summ = b + c
    else:
        summ = a + b + c
    return summ

"""    
d->vale, termina este ejercicio y nos vamos
    mañana quedamosa la misma hora?
a->ok
d->pues si quedamos mañana quiero que hagas lo de crear una funcion 
que haga algo, lo que tu elijas
a->okkkkkkkkkkk
d-> terminas?
a-> buzon moviestar
d-> te estoy llamando
"""

print(lone_sum(1,2,1))  # Should be 3
print(lone_sum(1,2,11)) # Should be 14
print(lone_sum(4,4,4))  # Should be 4

print(lone_sum(1,111,3))  # Should be 115
print(lone_sum(3,4,3))  # Should be 7
print(lone_sum(-1,4,4))  # Should be 3


"""
Mañana 21 de agosto:
 -> El programa que tu hagas
 -> Una funcion que sume todos los numeros en un intervalo,
        AMBOS INCLUSIVE  1, 4 es 10
 
def sum_intervalo(lim_inf, lim_sup): 
"""
print ("DEBERES:")
print ("")

def sum_intervalo(lim_inf,lim_sup):
    summ = 0
    for i in range(lim_inf, lim_sup+1):
        summ += i
    return summ

print(sum_intervalo(3,10))
print(sum_intervalo(-1, 3))

def vectdivisiblen(v, n):
#esta función devuelve todos los elementos de un vector divisibles por un número n en un nuevo vector
    d = []
    for i in range(len(v)):
        if v[i] != 0 and v[i] % n == 0:
            d.append(v[i])
    return d

v =[1, 2, 3, 6, 7, 9, 11, 12, 15, 18, 19, 21]
print(vectdivisiblen(v, 3))

h = [0, 1, 7865, 473, 2541, 6017, 666, 1573, 7254, 2222, 111, 3333, 9020, 44444]
print(vectdivisiblen(h, 11))

j = [99, 0, 81, 999, 27, 1134, 1, 4311, 4131, 6003, 44, 6300, 7, 3600, 2601, 2610]
print(vectdivisiblen(j, 9))

