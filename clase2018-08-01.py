
# Funciones: explicacion.


# Ejercicio 1
# Crea una función que te diga si un numero
# es par

# Ejercicio 2
# imprime una lista al revés

# Ejercicio 3
# Funcion que devuelve el mayor de 3 números

# Ejercicio 4
#  Question:
#  Write a program which will find all such numbers which are divisible by 7 but are not a multiple of 5,
#  between 2000 and 3200 (both included).
#  The numbers obtained should be printed in

# Ejercicio 5
# Jugando a piedra papel o tijera


# Ejercicio 6
# Crea una función que te diga si un número es primo



def funcion_test(a, b ,c):
    return a

def es_par(num):
    if num%2==0:
        return True
    else:
        return False

def incrementa(num):
    num += 1
    return num

def max_3(n1, n2, n3):
    print("el input es ", n1, n2, n3)
    t = max(n1, n2)
    t2 = max(t, n3)
    return t2


# 0 1 2 3 4 5 6 7  8  9

# 0 1 1 2 3 5 8 13 21 34


def fibonacci(n):
    if n == 0:
        return 0
    if n == 1:
        return 1
    val_n_1 = 1
    val_n_2 = 0
    
    for i in range (2,n):
        val_i = val_n_1 + val_n_2
        print("en la posición", i, "val_"+str(i)+" es", val_i)
        val_n_2 = val_n_1
        val_n_1 = val_i

  
    
a = 1
b = 5
c = max(a, b)
print(c)

# Estructura de las funciones
num = 7
es_par_bool = es_par(num)
print(es_par_bool)

# Las variables de argumento se pasan y se pueden modificar *NORMALMENTE*
# Por ejemplo los enteros no se cambian, mucho cuidado!!!!!
q = 2
q = incrementa(q) 
print(q)

# MAX 3 ----------> EJERCIO

pp = 50
rr = 15
qq = 9
max_3(pp, rr, qq)


coord_x_barco_1 = 0
coord_x_barco_2 = 4
coord_x_barco_3 = 12
t = max_3(coord_x_barco_1, coord_x_barco_2, coord_x_barco_3 )
print(t)


# BOOLEANO ----> str float int bool
hh = 7
mm = 5

mm = incrementa(incrementa(incrementa(mm)))

b1 = mm > hh
if b1:
    print(mm, "es mayor que", hh)
else:
    print(hh, "es mayor que", mm)

    
# int  = 0 -> bool False  // int != ->True
entero = 9
b2 = bool(entero)
b2 = entero!=0
# Es lo mishmo
# Esto siempre se imprime

if 1:
    print("Esto siempre se imprime")


# Imprime una cadena de caracteres str del revés. a = len("asfasd")  -> a vale 6 
# a = "sadfafa"  -> print(a[0]) -> imprime s
# a = "iiiii" -> a[1] = "q" -> a es "iqiii" -----> esto esta MAL NO SE PUEDE

cadena = "hola hermano esto no es un melíndromo"
cadena_resultado = ""

longi = len(cadena)
print(longi)
for i in range(longi):
    cadena_resultado = cadena_resultado + cadena[longi -1 -i] 
 
print(cadena_resultado)

# Fibbonacci Fn = Fn-1 + Fn-2 ---> F0 = 0 F1 = 1

n5 = fibonacci(15)





