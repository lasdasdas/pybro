def isprime(a):
    for i in range(2, a):
        if a%i==0:
            return False
    return True


#python 3.5.2
#  LISTAS

# Como crear una lista
list_0 = [] #Vacia
list_1 = [1.0, 2.0, 3.0, 3.25, 4.5]

list_2 = []
list_2.append(1.0)
list_2.append(2.0)
list_2.append(3.0)
list_2.append(3.25)
list_2.append(4.5)

# Como acceder a los elementos de un vector, imprime el primer elemento de list_1
print(list_2[0])

# Añadir elementos extra a una lista, añade el valor 8.0 al final de lista 1
list_1.append(8.0)

#Extra: añadir en una posicion cualquiera usando list_1.insert(index, value)
list_1.insert(3, -1.0)
print(list_1)

# Borrar elementos de una lista list_1.pop(0)-> borra el primer elemento de list1
list_1.pop(0)
print(list_1)


#Imprime la longitud de list_1
print(len(list_1))

# Acceder desde atrás: se usan subindices negativos list_1[-1] -> borra el último elemento
list_1.pop(-1)
print(list_1)

# Mezclar elementos: SE PUEDE 
list_mix = ["saf", 1.01, 1, ["adf", 21, 1231, "dfs"]]
print(list_mix)


# Ejercicio: crea una lista que contenga todos los numeros naturales del 0 al 15
vec_nat = []
for i in range(16):
    vec_nat.append(i)
print(vec_nat)

for i in range(len(vec_nat)):
    vec_nat[i] += 2
print(vec_nat)

# Ejercicio: mete en prime_vector todos los numeros primos del 1 al 50
prime_vector = []

for i in range(1, 10):
    if isprime(i):
        prime_vector.append(i)
print(prime_vector)



# Ejercicio, pon en una lista los 50 elementos de fibbonacci 

fibbo = [1 , 1]

for i in range(10):
    fibbo.append(fibbo[-1]+fibbo[-2])

# masgrande = 0
# [0, 5, 9, 1 , 3 , 7, 4, 78]
def maxvect(a):
    masgrande = a[0]
    for i in range(len(a)):
       if masgrande < a[i]:
            masgrande = a[i]
    return masgrande


vec_d_2 = [-6, -5, -9, -1 , -3 , -7, -4, -78]
print(maxvect(vec_d_2))

def has_five(v):
    for i in range(len(v)):
        if v[i] == 5:
            return True
    return False

vec_d_2 = [1, 5, 9, 1 , 3 , 7, 4, 78]

if has_five(vec_d_2):
    print("tiene 5")
else:
    print("no tiene 5")

def has_n(v, n):
    for i in range(len(v)):
        if v[i] == n:
            return True
    return False

if has_n(vec_d_2, 79):
    print("tiene 79")
else:
    print("no tiene 79")
    
def accumulate(v):
    acc = 0
    for i in range(len(v)):
        acc += v[i]
    return acc

suma = accumulate(vec_d_2)
print("la suma de vec_d_2 es", suma)

def accumulatemult(v):
    acc = 1
    for i in range(len(v)):
        acc *= v[i]
    return acc

mult = accumulatemult(vec_d_2)
print("la mult de vec_d_2 es", mult)
print("----------------------------------------")


# Un concepto sobre vectores 

mutable_vec = [1 , 2, 3, 4, 5, 6]
def square_vec(v):
    for i in range(len(v)):
        v[i] = v[i]*v[i]

        
print(mutable_vec)
square_vec(mutable_vec)
print(mutable_vec)
mut_vec_2 = mutable_vec.copy()
square_vec(mut_vec_2)
print(mutable_vec)


a = 1
def plusplus(n):
    n += 1
print(a)
plusplus(a)
print(a)


d = [1, 5, 9, 1 , 3 , 7, 1, 78, 9, 3, 89, 5, 6, 7, 9]

def duplicated_vec(v):
    dup = []
    for i in range(len(v)):
        for j in range(i+1, len(v)):
            if v[j] == v[i]:
                dup.append(v[i])
    return dup

a = duplicated_vec(d)
print(a)
            

def union(v1, v2):
    u = []
    for i in range(len(v1)):
        for j in range(len(v2)):
            if v1[i] == v2[j]:
                u.append(v1[i])
    return u
    
    
    """
    
    Returns a vector with all the common elements. 
    def has_n(v, n): te puede ayudar mucho. 
    
    """
   
b = [0, 1, 2, 4, 7]
c = [1, 2, 3, 5, 7]
unionvec = union(b, c)
print(unionvec)

