
v1 = [2, 1, 5, -80,0,15,-3, 1, 6, 7, -1]



# v1 = [2, 1, 5,0,15,-3, 1, 6, 7, -1]
# [-80, 



def minlist_pop(v):
    minv = v[0]
    minindex = 0
    for i in range(len(v)):
        if v[i] < minv:
            minv = v[i]
            minindex = i
    v.pop(minindex)
    return minv

def sort_list(l):
    s_l = []
    l_c = list.copy(l)
    for i in range(len(l_c)):
        minimo = minlist_pop(l_c)
        s_l.append(minimo)
    return s_l

sortedl = sort_list(v1)
print(sortedl)
print(v1)


#e = Sumatorio(k=0, k=inf) de la expresión (1 / k!)

def fact(n):
    """
    returns n!
    """
    acc = 1
    for j in range(1, n+1):
        acc *= j
    return acc

f = fact(0)
print(f)

def aprox_e(k_lim_sup):
    sumatorio = 0
    for i in range(k_lim_sup):
        sumatorio += 1/(fact(i))
    return sumatorio

e = aprox_e(30) 
print(e)

# 2.718281828459045235360287471352662497757247093699959574966967627724076630353
# 2.718281828459045            

import math
class Point:
    def __init__(self, x_arg, y_arg):
        self.x = x_arg
        self.y = y_arg
        
    def print_pretty(self):
        print("x , y : ",self.x,",", self.y)

        
    def distansia(self, pt2):
        dist = math.sqrt((self.x-pt2.x)**2 + (self.y-pt2.y)**2)
        return dist
    
    def mult(self, m):
        self.x *= m
        self.y *= m
    
    def dot(self, pt2):
        esc = self.x * pt2.x + self.y * pt2.y
        return esc
    
    def angulo(self):
        sen = self.y/(self.y**2 + self.x**2)
        angulo = math.asin(sen)*360/(2*math.pi)
        return angulo
        
pt1 = Point(5.2, 6.4)       
pt1.print_pretty()   
pt1.mult(3)
pt1.print_pretty()   


pt3 = Point(10.5, 30.2)
d = pt3.distansia(pt1)
print(d)
d = pt1.distansia(pt3)
print(d)

escalar = pt1.dot(pt3)
print(escalar)

pt4 = Point(7, 1)
pt5 = Point(1, 0)

ang = pt4.angulo()
print(ang)
  
def unomenosuno(n):
    for i in range(2, n+2):
        if i%2 == 0:
            print(1)
        else:
            print(-1)

def unomenosuno_2(n):
    for i in range(2, n+2):
        print((-1)**i)

unomenosuno_2(10)


# calculando pi
# sumatorio de i=0 a i=n de la expresión (-1)^i / (2*i +1)

def picalc(n):
    pi = 0
    for i in range(n):
        pi += (-1)**i / (2*i +1)
    return pi*4

print(picalc(10))
