
#        <=
#        >=
#        <
#        <
#        ==
#        !=

a = 789

if a % 7 == 0:
    print("es multiplo de 7")
elif a % 5 == 0:
    print("es multiplo de 5")
else:
    print("no es multiplo de 7")

# si el número es múltiplo de 3 o exactamente 7 es inválido 
# y le tengo que decir la razón pq lo  es

# var_1 = input("Meta el valor del código secreto") #esto da un str
var_1 = '151'
var_1 = int(var_1)
if var_1 % 3 == 0 or var_1 == 7:
    print("algo está mal")
    
if var_1 % 3 == 0:
    print("input inválido: var_1 es múltiplo de 3") 
if var_1 == 7 :
    print("input inválido: var_1 es exactamente 7")

edad = var_1

#mayor 20 adulto menor 20 niño
if edad>=20:
    print("eres un adulto")
elif edad>=12:
    print("eres un adolesente")
elif edad<12:
    print("eres un ninio")
else:
    print("no se que eres")

##################### CONTROL DE FLUJO 2

import random
r = random.randint(1,100)
# WHILE BREAK

print("#############################")

for i in range(100):
    r = random.randint(1,100)
    print(r)

print("#############################")

m = random.randint(1,100)
while m != 69:
    m = random.randint(1,100)
    print(m)

print("#############################")

z = random.randint(1, 10000)
for a in range(50):
    z = random.randint(1, 10000)
    if z%14 == 0:
        print("fatal error: mutiplo de 14")
        break
    else:
        print(a, z)
        
print("#############################")


"""        
0
1
    0
2
    1
    0
3
    2
    1
    0
4
    3
    2
    1
    0
5
    4
    3
    2
    1
    0
...
"""

for i in range(11):
    print(i)
    for j in range (0, i):
        print("    ", i-j-1)
