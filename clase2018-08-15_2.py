# Diccionarios
# ["hola"] -> llave


lis = ["Albert", 20, "M", True]


dic = {"nombre" : "Albert"}
dic["nombre"] = "Albert"
dic["edad"] = 20
dic["sexo"] = "M"
dic["sabe_python"] = True
dic["notas"] = [6, 8 , 9 , 5 , 4 ]

# print(lis[0], lis[2], lis[1])
# print(dic["nombre"], dic["sexo"])



class Alumno:
    def __init__(self):
        self.edad = 0
        self.nombre = ""
        self.sexo = ""
        self.estudios = ""
        self.notas = []

  
    def cumpleanios(self):
        self.edad += 1
        
    def print_pretty(self):
        print("nombre   :", self.nombre)
        print("edad     :", self.edad)
        print("sexo     :", self.sexo)
        print("estudios :", self.estudios)
        print("notas    :", self.notas)
        print("media    :", self.media())
        print("")
        
    def media(self):
        acc = 0
        for i in range(len(self.notas)):
            acc += self.notas[i]
        media = acc / len(self.notas)
        return media
                          
david = Alumno()
david.edad = 25
david.nombre = "David"
david.sexo = "genderfluid"
david.estudios = 99
david.notas = [10, 10, 10, 9, 9, 10, 4]
david.print_pretty()



albert = Alumno()
albert.edad = 20
albert.nombre = "albertusinho"
albert.sexo = "no te importa cis scum"
albert.estudios = 100
albert.notas = [10, 10, 10, 9, 9, 10, 10]


albert.cumpleanios()
albert.media()
albert.print_pretty()

pedro = Alumno()
pedro.edad = 21
pedro.nombre = "pedro"
pedro.sexo = "no te importa cis scum"
pedro.estudios = 12
pedro.notas = [3, 4, 0, 9, 9, 10, 10]
pedro.print_pretty()


clase_2018 = [albert, david, pedro]

