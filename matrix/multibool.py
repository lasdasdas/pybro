A = [[1, 0, 0],
     [0, 1, 0],
     [0, 0, 1]]
B = [[1, 2, 3],
     [4, 5, 6],
     [7, 8, 9]]


#len(A) = m ---> número de filas
#len(A[0]) = n ---> número de columnas

def matrixsize(mat):
#determina la dimension de una matriz
    return [len(mat), len(mat[0])]

def matrixsize2(mat):
#determina la dimension de una matriz
    return len(mat), len(mat[0])

#m, n = matrixsize2(A)


def multibool(mat1, mat2):
#determina si dos matrices se pueden multiplicar
    dim1 = matrixsize(mat1)
    dim2 = matrixsize(mat2)
    return (dim1[1] == dim2[0])

def productdim(mat1, mat2):
#determina la dimensión de la matriz que resulta de un producto de matrices
    if(multibool(mat1, mat2)):
        dim1 = matrixsize(mat1)
        dim2 = matrixsize(mat2)
        return [dim1[0], dim2[1]]
    else:
        return "estas matrices no se pueden multiplicar"

def element(mat1, mat2, i, j):
#calcula el elemento i,j del producto de dos matrices
    if(multibool(mat1, mat2) == True):
        m1, n1 = matrixsize(mat1)
        m2, n2 = matrixsize(mat2)
        suma = 0
        for p in range(len(mat2[0])):
            suma += mat1[i][p] * mat2[j][p]
        return suma
    else:
        return "estas matrices no se pueden multiplicar"

def matrixproduct(mat1, mat2):
#calcula el producto de dos matrices
    dimvec = productdim(mat1, mat2)
    result = []
    for i in range(len(mat1[0])):
        for j in range(len(mat2)):
            e = element(mat1, mat2, i, j)
            result.append(e)
    print(result)

matrixproduct(A, B)
print(element(A, B, 1, 1))
#print(productdim(A, B))
#print(productdim(A, C))
